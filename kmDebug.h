/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//** @file
* @mainpage 
* @section pageTOC Content
* @brief Debug and troubleshooting library functions for AVR MCUs.
* - kmDebug.h
* - kmDebugDefaultConfig.h
*
*  **Created on**: Aug 10, 2019 @n
*      **Author**: Krzysztof Moskwa @n
*      **License**: GPL-3.0-or-later @n@n
*
*  kmDebug library for AVR MCUs @n
*  **Copyright (C) 2019  Krzysztof Moskwa**
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*  Example usage:
@code
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <util/delay.h>

#include "kmDebug/kmDebug.h"
#include "kmDebug/kmDebugDefaultConfig.h"

int main(void) {
	dbPullUpAllPorts();
	dbInit();
	dbOff(DB_PIN_0);

	while(true) {
		dbToggle(DB_PIN_0);
		_delay_ms(500);
	}
}
@endcode
*  References:
* -# https://atnel2.blogspot.com/2014/04/puapki-programowe-debuger-na-jednej.html

*/

#ifndef KM_DEBUG_H_
#define KM_DEBUG_H_
#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

#include "../kmCommon/kmCommon.h"

/// Definition of the kmDebug internal pin number, allows to remap the pins without changing the rest of the code. See kmDebugDefaultConfig.h for details
typedef enum {
	  DB_PIN_0
/// Debug pin 0
#ifdef KM_DEBUG_PIN_1
	, DB_PIN_1
/// Debug pin 1
#endif
#ifdef KM_DEBUG_PIN_2
	, DB_PIN_2
/// Debug pin 2
#endif
#ifdef KM_DEBUG_PIN_3
	, DB_PIN_3
/// Debug pin 3
#endif
#ifdef KM_DEBUG_PIN_4
	, DB_PIN_4
/// Debug pin 4
#endif
#ifdef KM_DEBUG_PIN_5
	, DB_PIN_5
/// Debug pin 5
#endif
#ifdef KM_DEBUG_PIN_6
	, DB_PIN_6
/// Debug pin 6
#endif
#ifdef KM_DEBUG_PIN_7
	, DB_PIN_7
/// Debug pin 7
#endif
} kmDebugPin;

/**
Initializes all ports to pull-up state.
\b NOTE: This method should be issued first in the main routine
*/
void dbPullUpAllPorts(void);

/**
Initializes the debug pins with HIGH state.
Following definitions to be set in config.h file @n
#define \b KM_DEBUG_DDR data direction register for debug pin (e.g DDRC) @n
#define \b KM_DEBUG_PORT debug pin port (e.g PORTC) @n
#define \b KM_DEBUG_PIN_X debug pin (e.g PC1) @n
#define \b KM_DEBUG_INVERT_LOGIC if defined, then dbOn sets the pin to LOW, and dbOff to HIGH ) @n
#define \b KM_DEBUG_DISABLE - to disable debug feature completely @n
*/
void dbInit(void);

/**
Issues next debug by toggling the debug pin of specific number.
@param pin Pin number from 0 to 7. Note that only pins that have defined DEBUG_PINX are active.
*/
void dbToggle(const kmDebugPin pin);

/**
Issues debug by switching on the debug pin of specific number.
@param pin Pin number from 0 to 7. Note that only pins that have defined DEBUG_PINX are active.
*/
void dbOn(const kmDebugPin pin);

/**
Issues debug by switching off the debug pin of specific number.
@param pin Pin number from 0 to 7. Note that only pins that have defined DEBUG_PINX are active.
*/
void dbOff(const kmDebugPin pin);

/**
Issues debug by switching on or off the debug pin of specific number depends on the state parameter
@param pin Pin number from 0 to 7. Note that only pins that have defined DEBUG_PINX are active.
@param state if true switches the debug pin ON, OFF otherwise
*/
void dbSet(const kmDebugPin pin, const bool state);

#ifdef __cplusplus
}
#endif
#endif /* KM_DEBUG_H_ */
