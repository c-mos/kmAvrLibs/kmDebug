// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
/** @file
* @brief Default configuration options that can be overridden in main config file
* kmDebugDefaultConfig.h
*
*  **Created on**: Aug 10, 2019 @n
*      **Author**: Krzysztof Moskwa @n
*      **License**: GPL-3.0-or-later @n@n
*
*  kmDebug library for AVR MCUs @n
*  **Copyright (C) 2019  Krzysztof Moskwa**
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*  Note: Use following definition to disable kmDebug library #define KM_DEBUG_DISABLE (it will not be compiled into target binary code)
*
*  References:
* -# https://atnel2.blogspot.com/2014/04/puapki-programowe-debuger-na-jednej.html
*/

#ifndef KM_DEBUG_DEFAULT_CONFIG_H_
#define KM_DEBUG_DEFAULT_CONFIG_H_

#include "../kmCommon/kmCommon.h"

//#define KM_DEBUG_DISABLE

/// Inverts output logic so dbOn sets the pin to LOW and dbOff sets the pin to HIGH (if defined)
#define KM_DEBUG_INVERTED_LOGIC

/// Defines MCUs direction register to be used by the library
#define KM_DEBUG_DDR DDRB
/// Defines MCUs port register to be used in the library
#define KM_DEBUG_PORT PORTB
/// KM_DEBUG_PIN_n defines particular pin to be used for DB_PIN_n selection 
#define KM_DEBUG_PIN_0 PB5
//#define KM_DEBUG_PIN_1 PB1
//#define KM_DEBUG_PIN_2 PB2
//#define KM_DEBUG_PIN_3 PB3
//#define KM_DEBUG_PIN_4 PB4
//#define KM_DEBUG_PIN_5 PB5
//#define KM_DEBUG_PIN_6 PB6
//#define KM_DEBUG_PIN_7 PB7

#endif /* KM_DEBUG_DEFAULT_CONFIG_H_ */
