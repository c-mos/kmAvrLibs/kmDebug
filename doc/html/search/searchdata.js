var indexSectionsWithContent =
{
  0: "cdkr",
  1: "k",
  2: "d",
  3: "k",
  4: "d",
  5: "k",
  6: "kr"
};

var indexSectionNames =
{
  0: "all",
  1: "files",
  2: "functions",
  3: "enums",
  4: "enumvalues",
  5: "defines",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Files",
  2: "Functions",
  3: "Enumerations",
  4: "Enumerator",
  5: "Macros",
  6: "Pages"
};

