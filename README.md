# ReadMe
# kmDebug library for AVR MCUs

This repository contains the kmDebug library, a debug and troubleshooting library for AVR MCUs.

## Table of Contents
- [Version History](#version-history)
- [Overview](#overview)
- [Dependencies](#dependencies)
- [Usage](#usage)
- [Example Code](#example-code)
- [Author and License](#author-and-license)

## Version History
v1.0.0 Initial (2024-05-25)

## Overview
The `kmDebug` library provides functions to facilitate debugging and troubleshooting on AVR microcontrollers.

### Features

- Initialization of debug pins
- Toggling debug pins
- Turning debug pins on and off
- Customizable debug pin configurations


## Dependencies
- [kmCommon](https://gitlab.com/c-mos/kmAvrLibs/kmCommon)

## Usage
Getting this library and adding it to own project:
- To add this module to own project as submodule - enter the main directory of the source code and use git command

``` bash
git submodule add git@gitlab.com:c-mos/kmAvrLibs/kmDebug.git kmDebug
```
- After cloning own application from git repository use following additional git command to get correct revision of submodule:
``` bash
git submodule update --init
```

### Configuration

The default configuration options for `kmDebug` can be overridden in the main config file `kmDebugDefaultConfig.h`. Below are some of the configurable options:

- Inverted output logic
- Definition of MCU's direction register and port register
- Definition of debug pins


## Example Code
```c
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <util/delay.h>

#include "kmDebug/kmDebug.h"
#include "kmDebug/kmDebugDefaultConfig.h"

int main(void) {
    dbPullUpAllPorts()
    dbInit()
    dbOff(DB_PIN_0)

    while(true) {
        dbToggle(DB_PIN_0)
        _delay_ms(500)
    }
}

```

See Also:
[kmDebug Example Test Application](https://gitlab.com/c-mos/kmAvrTests/kmDebugTest)
### References:
- [Blog Post on Debugging Techniques (PL)](https://atnel2.blogspot.com/2014/04/puapki-programowe-debuger-na-jednej.html)

## Author and License
Author: Krzysztof Moskwa

e-mail: chris[dot]moskva[at]gmail[dot]com

Software License: GNU General Public License (GPL) version 3.0 or later. See [LICENSE.txt](https://www.gnu.org/licenses/gpl-3.0.txt)

 ![GPL3 Logo](https://www.gnu.org/graphics/gplv3-or-later-sm.png)