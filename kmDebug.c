/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//*
* kmDebug.c
*
*  Created on: Jul 10, 2019
*      Author: Krzysztof Moskwa
*      License: GPL-3.0-or-later
*
*  kmDebug library for AVR MCUs
*  Copyright (C) 2019  Krzysztof Moskwa
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/

#include "kmDebug.h"

#include <avr/io.h>

#define KM_DEBUG_ALL_PINS_AS_IN		0x00u
#define KM_DEBUG_ALL_PINS_AS_HIGH	0xffu

void dbPullUpAllPorts(void) {
	// PULL UP all ports
#ifdef PORTA
	DDRA  = KM_DEBUG_ALL_PINS_AS_IN;
	PORTA = KM_DEBUG_ALL_PINS_AS_HIGH;
#endif /* PORTA */
#ifdef PORTB
	DDRB  = KM_DEBUG_ALL_PINS_AS_IN;
	PORTB = KM_DEBUG_ALL_PINS_AS_HIGH;
#endif /* PORTB */
#ifdef PORTC
	DDRC  = KM_DEBUG_ALL_PINS_AS_IN;
	PORTC = KM_DEBUG_ALL_PINS_AS_HIGH;
#endif /* PORTC */
#ifdef PORTD
	DDRD  = KM_DEBUG_ALL_PINS_AS_IN;
	PORTD = KM_DEBUG_ALL_PINS_AS_HIGH;
#endif /* PORTD */
#ifdef PORTE
	DDRE  = KM_DEBUG_ALL_PINS_AS_IN;
	PORTE = KM_DEBUG_ALL_PINS_AS_HIGH;
#endif /* PORTE */
#ifdef PORTF
	DDRF  = KM_DEBUG_ALL_PINS_AS_IN;
	PORTF = KM_DEBUG_ALL_PINS_AS_HIGH;
#endif /* PORTF */
#ifdef PORTG
	DDRG  = KM_DEBUG_ALL_PINS_AS_IN;
	PORTG = KM_DEBUG_ALL_PINS_AS_HIGH;
#endif /* PORTG */
#ifdef PORTH
	DDRH  = KM_DEBUG_ALL_PINS_AS_IN;
	PORTH = KM_DEBUG_ALL_PINS_AS_HIGH;
#endif /* PORTH */
#ifdef PORTI
	DDRI  = KM_DEBUG_ALL_PINS_AS_IN;
	PORTI = KM_DEBUG_ALL_PINS_AS_HIGH;
#endif /* PORTI */
#ifdef PORTJ
	DDRJ  = KM_DEBUG_ALL_PINS_AS_IN;
	PORTJ = KM_DEBUG_ALL_PINS_AS_HIGH;
#endif /* PORTJ */
#ifdef PORTK
	DDRK  = KM_DEBUG_ALL_PINS_AS_IN;
	PORTK = KM_DEBUG_ALL_PINS_AS_HIGH;
#endif /* PORTK */
}

void dbInit(void) {
#ifndef KM_DEBUG_DISABLE
	KM_DEBUG_DDR |= _BV(KM_DEBUG_PIN_0);
#ifdef KM_DEBUG_PIN_1
	KM_DEBUG_DDR |= _BV(KM_DEBUG_PIN_1);
#endif /* KM_DEBUG_PIN_1 */
#ifdef KM_DEBUG_PIN_2
	KM_DEBUG_DDR |= _BV(KM_DEBUG_PIN_2);
#endif /* KM_DEBUG_PIN_2 */
#ifdef KM_DEBUG_PIN_3
	KM_DEBUG_DDR |= _BV(KM_DEBUG_PIN_3);
#endif /* KM_DEBUG_PIN_3 */
#ifdef KM_DEBUG_PIN_4
	KM_DEBUG_DDR |= _BV(KM_DEBUG_PIN_4);
#endif /* KM_DEBUG_PIN_4 */
#ifdef KM_DEBUG_PIN_5
	KM_DEBUG_DDR |= _BV(KM_DEBUG_PIN_5);
#endif /* KM_DEBUG_PIN_5 */
#ifdef KM_DEBUG_PIN_6
	KM_DEBUG_DDR |= _BV(KM_DEBUG_PIN_6);
#endif /* KM_DEBUG_PIN_6 */
#ifdef KM_DEBUG_PIN_7
	KM_DEBUG_DDR |= _BV(KM_DEBUG_PIN_7);
#endif /* KM_DEBUG_PIN_7 */
#endif
}

void dbToggle(const kmDebugPin pin) {
#ifndef KM_DEBUG_DISABLE
	switch(pin) {
		case DB_PIN_0: {
			KM_DEBUG_PORT ^= _BV(KM_DEBUG_PIN_0);
			break;
		}
#ifdef KM_DEBUG_PIN_1
		case DB_PIN_1: {
			KM_DEBUG_PORT ^= _BV(KM_DEBUG_PIN_1);
			break;
		}
#endif /* KM_DEBUG_PIN_1 */
#ifdef KM_DEBUG_PIN_2
		case DB_PIN_2: {
			KM_DEBUG_PORT ^= _BV(KM_DEBUG_PIN_2);
			break;
		}
#endif /* KM_DEBUG_PIN_2 */
#ifdef KM_DEBUG_PIN_3
		case DB_PIN_3: {
			KM_DEBUG_PORT ^= _BV(KM_DEBUG_PIN_3);
			break;
		}
#endif /* KM_DEBUG_PIN_3 */
#ifdef KM_DEBUG_PIN_4
		case DB_PIN_4: {
			KM_DEBUG_PORT ^= _BV(KM_DEBUG_PIN_4);
			break;
		}
#endif /* KM_DEBUG_PIN_4 */
#ifdef KM_DEBUG_PIN_5
		case DB_PIN_5: {
			KM_DEBUG_PORT ^= _BV(KM_DEBUG_PIN_5);
			break;
		}
#endif /* KM_DEBUG_PIN_5 */
#ifdef KM_DEBUG_PIN_6
		case DB_PIN_6: {
			KM_DEBUG_PORT ^= _BV(KM_DEBUG_PIN_6);
			break;
		}
#endif /* KM_DEBUG_PIN_6 */
#ifdef KM_DEBUG_PIN_7
		case DB_PIN_7: {
			KM_DEBUG_PORT ^= _BV(KM_DEBUG_PIN_7);
			break;
		}
#endif /* KM_DEBUG_PIN_7 */
	}
#endif /* KM_DEBUG_DISABLE */
}

#ifdef KM_DEBUG_INVERTED_LOGIC
void dbOn(const kmDebugPin pin) {
#else
void dbOff(const kmDebugPin pin) {
#endif
#ifndef KM_DEBUG_DISABLE
	switch(pin) {
		case DB_PIN_0: {
			KM_DEBUG_PORT &= ~_BV(KM_DEBUG_PIN_0);
			break;
		}
#ifdef KM_DEBUG_PIN_1
		case DB_PIN_1: {
			KM_DEBUG_PORT &= ~_BV(KM_DEBUG_PIN_1);
			break;
		}
#endif /* KM_DEBUG_PIN_1 */
#ifdef KM_DEBUG_PIN_2
		case DB_PIN_2: {
			KM_DEBUG_PORT &= ~_BV(KM_DEBUG_PIN_2);
			break;
		}
#endif /* KM_DEBUG_PIN_2 */
#ifdef KM_DEBUG_PIN_3
		case DB_PIN_3: {
			KM_DEBUG_PORT &= ~_BV(KM_DEBUG_PIN_3);
			break;
		}
#endif /* KM_DEBUG_PIN_3 */
#ifdef KM_DEBUG_PIN_4
		case DB_PIN_4: {
			KM_DEBUG_PORT &= ~_BV(KM_DEBUG_PIN_4);
			break;
		}
#endif /* KM_DEBUG_PIN_4 */
#ifdef KM_DEBUG_PIN_5
		case DB_PIN_5: {
			KM_DEBUG_PORT &= ~_BV(KM_DEBUG_PIN_5);
			break;
		}
#endif /* KM_DEBUG_PIN_5 */
#ifdef KM_DEBUG_PIN_6
		case DB_PIN_6: {
			KM_DEBUG_PORT &= ~_BV(KM_DEBUG_PIN_6);
			break;
		}
#endif /* KM_DEBUG_PIN_6 */
#ifdef KM_DEBUG_PIN_7
		case DB_PIN_7: {
			KM_DEBUG_PORT &= ~_BV(KM_DEBUG_PIN_7);
			break;
		}
#endif /* KM_DEBUG_PIN_7 */
	}
#endif /* KM_DEBUG_DISABLE */
}

#ifdef KM_DEBUG_INVERTED_LOGIC
void dbOff(const kmDebugPin pin) {
#else
void dbOn(const kmDebugPin pin) {
#endif
#ifndef KM_DEBUG_DISABLE
	switch(pin) {
		case DB_PIN_0: {
			KM_DEBUG_PORT |= _BV(KM_DEBUG_PIN_0);
			break;
		}
#ifdef KM_DEBUG_PIN_1
		case DB_PIN_1: {
			KM_DEBUG_PORT |= _BV(KM_DEBUG_PIN_1);
			break;
		}
#endif /* KM_DEBUG_PIN_1 */
#ifdef KM_DEBUG_PIN_2
		case DB_PIN_2: {
			KM_DEBUG_PORT |= _BV(KM_DEBUG_PIN_2);
			break;
		}
#endif /* KM_DEBUG_PIN_2 */
#ifdef KM_DEBUG_PIN_3
		case DB_PIN_3: {
			KM_DEBUG_PORT |= _BV(KM_DEBUG_PIN_3);
			break;
		}
#endif /* KM_DEBUG_PIN_3 */
#ifdef KM_DEBUG_PIN_4
		case DB_PIN_4: {
			KM_DEBUG_PORT |= _BV(KM_DEBUG_PIN_4);
			break;
		}
#endif /* KM_DEBUG_PIN_4 */
#ifdef KM_DEBUG_PIN_5
		case DB_PIN_5: {
			KM_DEBUG_PORT |= _BV(KM_DEBUG_PIN_5);
			break;
		}
#endif /* KM_DEBUG_PIN_5 */
#ifdef KM_DEBUG_PIN_6
		case DB_PIN_6: {
			KM_DEBUG_PORT |= _BV(KM_DEBUG_PIN_6);
			break;
		}
#endif /* KM_DEBUG_PIN_6 */
#ifdef KM_DEBUG_PIN_7
		case DB_PIN_7: {
			KM_DEBUG_PORT |= _BV(KM_DEBUG_PIN_7);
			break;
		}
#endif /* KM_DEBUG_PIN_7 */
	}
#endif /* KM_DEBUG_DISABLE */
}

void dbSet(const kmDebugPin pin, const bool state) {
#ifndef KM_DEBUG_DISABLE
	if (state) {
		dbOn(pin);
	} else {
		dbOff(pin);
	}
#endif /* KM_DEBUG_DISABLE */
}

